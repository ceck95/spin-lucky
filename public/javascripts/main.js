$(document).ready(function() {

  function parseCode(i) {
    var str = '' + (i),
      pad = [];
    data.countColumns.forEach(function() {
      pad.push('0');
    });

    pad = pad.join('');

    var ans = pad.substring(0, pad.length - str.length) + str;
    return ans;
  }

  function loop(index, callback) {
    if (index >= 10)
      return callback();
    var items = $('.slots li'),
      itemClass = items.find('.available');

    items.each(function(i) {
      var item = $(this).find('.available');
      item.html(index);
      // item.addClass('animation-spin');
    });
    itemClass.addClass('animation-spin');
    setTimeout(function() {
      // if (index === 10) {
      itemClass.removeClass('animation-spin');
      setTimeout(function() {
        loop(++index, callback);
      }, 50);
      // }
    }, 50);

  }

  var interval = null,
    stopped = false,
    setIntervalSetValue = null,
    currentCode = null;

  $(".btn-start").click(function() {
    clearInterval(setIntervalSetValue);
    setIntervalSetValue = null;
    interval = setInterval(function() {
      return loop(1, function() {
        if (stopped) {
          interval = clearInterval(interval);
          interval = null;
          currentCode = list[(Math.floor(Math.random() * list.length) + 0)];
          console.log(currentCode);
          var listCode = parseCode(currentCode.code).split(''),
            nameCode = currentCode.name;

          setIntervalSetValue = setInterval(function() {
            $('.slots li').each(function(i) {
              $(this).find('.available').text(listCode[i]).fadeIn(2000);
            });
          }, 1);
          setTimeout(function() {
            $('.text-spin').text(nameCode);
            $('.btn-confirm').removeClass('hidden');
            $('.btn-reject').removeClass('hidden');
          }, 1000);
          stopped = false;
        }
      });
    }, 800);
    $(this).addClass('hidden');
    $('.text-spin').text('Winner is coming ...');
    setTimeout(function() {
      $(".btn-stopped").removeClass('hidden');
    }, 800);
  });

  $('.btn-confirm').click(function() {
    $('.btn-confirm').attr('disabled');
    $('.btn-reject').attr('disabled');
    $.ajax({
      url: '/api/list-winner',
      type: 'PUT',
      dataType: 'json',
      data: {
        listCodeId: currentCode.id,
        name: currentCode.name,
        nameCompetition: data.appName
      }
    }).done(function(resp) {
      clearInterval(setIntervalSetValue);
      $('.btn-confirm').removeAttr('disabled');
      $('.btn-reject').removeAttr('disabled');
      $('.btn-confirm').addClass('hidden');
      $('.btn-reject').addClass('hidden');
      $(".btn-start").removeClass('hidden');
      $('.text-spin').text('Press the Spin button to start');
      $('.slots li').each(function(i) {
        $(this).find('.available').html('<img src="images/bronze-prize.svg">');
      });
    });
  });

  $('.btn-reject').click(function() {
    clearInterval(setIntervalSetValue);
    $('.btn-confirm').addClass('hidden');
    $('.btn-reject').addClass('hidden');
    $(".btn-start").removeClass('hidden');
    $('.text-spin').text('Press the Spin button to start');
    $('.slots li').each(function(i) {
      $(this).find('.available').html('<img src="images/bronze-prize.svg">');
    });
  });

  $(".btn-stopped").click(function() {
    stopped = true;
    $(this).addClass('hidden');
    // $(".btn-start").removeClass('hidden');
    // $('.text-spin').text('Press the Spin button to start');
  });

  function toggleResult() {
    var selectorModal = $('.animation-modal-x');
    if (!selectorModal.hasClass('in')) {
      selectorModal.addClass('in');
    } else {
      selectorModal.removeClass('in');
    }
  }

  function parseDate(date) {
    var e = new Date(date);
    return `${e.getHours()}:${e.getMinutes()}:${e.getSeconds()} ${e.getDate()}/${e.getMonth()}/${e.getFullYear()}`
  }

  function getListWinner() {
    $.ajax({
      url: '/api/list-winner',
      type: 'GET',
      dataType: 'json'
    }).done(function(resp) {
      var data = resp.data,
        result = [];
      data.forEach(e => {
        result.push(`<tr><td>${parseCode(e.listCode.code)}</td><td>${e.name}</td><td>${parseDate(e.createdAt)}</td></tr>`)
      });

      $('.table-history tbody').html(result.join(''));
    });
  }

  $('.toggle-result').click(function() {
    getListWinner();
    toggleResult();
  });

  $('.close').click(function() {
    toggleResult();
  });

});