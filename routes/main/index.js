const Route = require('core-express').Route;
const indexController = require('../../controllers/index');
const routeIndex = new Route({
  basePath: '/',
  controller: indexController
});

routeIndex.addRoute({
  url: '/',
  method: 'GET',
  middleware: 'index'
});

module.exports = routeIndex.route;