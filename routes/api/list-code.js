const Route = require('core-express').Route;
const listCodeController = require('../../controllers/api/list-code');
const routeListCode = new Route({
  basePath: '/list-code',
  controller: listCodeController
});

routeListCode.addRoute({
  url: '/list-code',
  method: 'GET',
  summary: 'Insert list code',
  description: 'Returns list code',
  middleware: 'insertMany'
});

module.exports = routeListCode.route;