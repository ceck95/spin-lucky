const Route = require('core-express').Route;
const listWinnerController = require('../../controllers/api/list-winner');
const schemaListWinner = require('../../schemas/api/list-winner');
const route = new Route({
  basePath: '/list-winner',
  controller: listWinnerController,
  storeName: 'ListWinner',
  includes: ['insert'],
  schema: schemaListWinner
});

route.addRoute({
  url: '/list-winner',
  method: 'GET',
  summary: 'Get list winner',
  description: 'Returns list winner',
  middleware: 'getMany'
});


module.exports = route.route;