const CoreExpress = require('core-express');
const configServer = require('./config/config-server');
const ServerExpress = new CoreExpress.Server(configServer);

ServerExpress.runServer();