const Joi = require('joi');

const insert = {
  body: {
    listCodeId: Joi.number().required(),
    name: Joi.string().required(),
    nameCompetition: Joi.string().required()
  }
};

module.exports = {
  insert: insert
};