const BaseController = require('core-express').Controller;
const BPromise = require('bluebird');
const fs = BPromise.promisifyAll(require('fs'));

class ListCodeController extends BaseController {

  insertMany(req, res) {
    return fs.readFileAsync(`${process.cwd()}/data.txt`, 'utf8').then(content => {
      let data = [];
      content = JSON.parse(`["${content.split('\n').join('","')}"]`).map(e => {
        e = e.trim();
        return e;
      }).forEach((e, i) => {
        data.push({
          name: e,
          code: (i + 1),
        });
      });
      const listCodeStore = req.getStore('ListCode');
      return listCodeStore.insert(data).then(() => {
        return req.reply(req, res, {}, {
          message: 'Insert list code successfully'
        });
      }).catch(err => {
        return req.replyError(req, res, err);
      });

    }).catch(err => {
      return req.reply(req, res, err);
    });
  }

}

module.exports = ListCodeController;