const BaseController = require('core-express').Controller;

class ListWinnerController extends BaseController {

  getMany(req, res) {
    const listWinnerStore = req.getStore('ListWinner'),
      listCodeStore = req.getStore('ListCode');
    listWinnerStore.filter({}, {
      includes: [{
        storeModel: listCodeStore,
        columns: ['id', 'listCodeId']
      }],
      getMany: true,
      order: '-createdAt'
    }).then(listWinner => {
      return req.reply(req, res, listWinner, {
        message: 'Get list winner successfully'
      });
    }).catch(err => {
      return req.replyError(req, res, err);
    });
  }

}

module.exports = ListWinnerController;