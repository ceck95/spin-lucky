const BaseController = require('core-express').Controller;
const helper = require('helpers');

class IndexController extends BaseController {

  index(req, res) {
    const settingStore = req.getStore('Setting'),
      listCodeStore = req.getStore('ListCode');
    return settingStore.filter({}, {
      getMany: true
    }).then(setting => {
      let resp = {};
      setting.forEach(e => {
        resp[helper.String.toCamelCase(e.metaKey)] = e.metaValue;
      });
      const rowColumn = resp.countColumns;
      resp.countColumns = [];
      for (let i = 0; i < rowColumn; i++) {
        resp.countColumns.push(i);
      }
      return listCodeStore.filter({}, {
        getMany: true
      }).then(listCode => {
        resp.listCode = listCode;
        return req.reply(req, res, {
          data: resp
        }, 'index');
      }).catch(err => {
        return req.replyError(req, res, err);
      });
    }).catch(err => {
      return req.replyError(req, res, err);
    });
  }

}

module.exports = IndexController;