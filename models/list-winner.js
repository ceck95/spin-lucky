const BaseModel = require('core-mysql').BaseModel;

class ListWinner extends BaseModel {

  get table() {
    return 'list_winner';
  }

}

module.exports = ListWinner;