const BaseModel = require('core-mysql').BaseModel;

class ListCode extends BaseModel {

  get table() {
    return 'list_code';
  }

}

module.exports = ListCode;