const BaseModel = require('core-mysql').BaseModel;

class Setting extends BaseModel {

  get table() {
    return 'setting';
  }

}

module.exports = Setting;