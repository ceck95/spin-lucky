CREATE TABLE sl_setting(
	id int NOT NULL AUTO_INCREMENT,
	meta_key text,
	meta_value text,
  description text,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE TABLE sl_list_code(
	id int NOT NULL AUTO_INCREMENT,
	name text,
  code text,
  description text,
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE TABLE sl_list_winner(
	id int NOT NULL AUTO_INCREMENT,
	list_code_id int,
  name text,
  name_competition text,
	updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
  FOREIGN KEY (list_code_id) REFERENCES sl_list_code(id)
);